def sumar(a, b):
    return a + b


def restar(a, b):
    return a - b


print('la suma de {} + {} = {}'.format(1,  2, sumar(1, 2)))
print('la suma de {} + {} = {}'.format(3,  4, sumar(3, 4)))

print('la resta de {} - {} = {}'.format(6,  5, restar(6, 5)))
print('la resta de {} - {} = {}'.format(8,  7, restar(8, 7)))

